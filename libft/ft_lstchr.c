/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 15:59:04 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/19 19:09:13 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstchr(t_list **list, char c)
{
	t_list	*ptr;

	if (!*list)
		return (NULL);
	ptr = *list;
	while (ptr)
	{
		if (ft_strchr((char *)ptr->content, c))
			return (ptr);
		ptr = ptr->next;
	}
	return (NULL);
}
