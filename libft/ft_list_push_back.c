/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/23 19:37:10 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/19 15:52:02 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_list_push_back(t_list **begin_list, void *content,
		size_t content_size)
{
	t_list	*to_push;
	t_list	*tmp;

	if ((to_push = ft_lstnew(content, content_size)) == NULL)
		return ;
	if (*begin_list == NULL)
		*begin_list = to_push;
	else
	{
		tmp = *begin_list;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = to_push;
	}
}
