/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_cut_head.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 15:50:46 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/24 23:04:02 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_list_cut_head(t_list **head, t_list *new)
{
	t_list	*ptr;
	t_list	*tmp;

	if ((ptr = *head) == NULL)
		return ;
	while (ptr != new)
	{
		tmp = ptr->next;
		free(ptr->content);
		free(ptr);
		ptr = tmp;
		if (ptr == NULL)
		{
			*head = NULL;
			return ;
		}
	}
	*head = ptr->next;
	free(ptr->content);
	free(ptr);
}
