#include "libft/libft.h"
#include "get_next_line.h"
#include <fcntl.h>
#include <stdio.h>

int		main(int argc, char **argv)
{
	int		fd;
	char	*line;
	int		ret;

	ret = 1;
	(void)argc;
	fd = open(argv[1], O_RDONLY);
	ret = get_next_line(fd, &line);
	printf("%s\n", line);
	ret = get_next_line(fd, &line);
	printf("%s\n", line);
	ret = get_next_line(fd, &line);
	printf("%s\n", line);
	ret = get_next_line(fd, &line);
	printf("%s\n", line);
	ret = get_next_line(fd, &line);
	printf("%s\n", line);
	return (0);
}
